from typing import List, Optional

from fastapi import Depends, FastAPI, HTTPException, status
from sqlalchemy.orm import Session

#import crud, model, schema
#import crud
#import model
#import schema

#import crud, model, schema
from . import crud # DEJARLO CON EL PUNTO Y EJECUTAR uvicorn modeloDatos.main:app --reload
from . import model # EL USUARIO NO PUEDE MODIFICAR SU EMAIL
from . import schema

from .database import SessionLocal, engine
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm # QUITAR OAUTH2
from datetime import timedelta, datetime

from jose import JWTError, jwt

model.Base.metadata.create_all(bind=engine)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
TIEMPO_EXPIRACION = 30

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()



#@app.get("/token/administrador", response_model = schema.AdministradorFull)
#async def get_administrador_login(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
#    credentials_exception = HTTPException(
#        status_code=status.HTTP_401_UNAUTHORIZED,
#        detail="Credenciales erróneas",
#        headers={"WWW-Authenticate": "Bearer"},
#    )
#    try:
#        payload = crud.decodeToken(token)
#        usuario: str = payload.get("sub")
#        if usuario is None:
#            raise credentials_exception
#        token_data = schema.TokenData(usuario=usuario)
#    except JWTError:
#        raise credentials_exception
#    administrador = getAdministradorCorreo(db, correo=token_data.cliente)
#    if administrador is None:
#        raise credentials_exception
#    return administrador


#@app.get("/token/cliente", response_model = schema.ClienteFull)
async def get_usuario_login(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    print("token antes decode: ", token)
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Credenciales erróneas",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        print("token decode: ", token)
        payload = crud.decodeToken(token)
        usuario: str = payload.get("sub")
        rol: str = payload.get("rol")
        if usuario is None:
            raise credentials_exception
        token_data = schema.TokenData(usuario=usuario)
    except JWTError:
        raise credentials_exception
    user = crud.getUserCorreo(db, correo=token_data.usuario)
    #if user is None:
    #    user = crud.getConductorCorreo(db, correo=token_data.usuario)
    #    if user is None:
    #        user = crud.getAdministradorCorreo(db, correo=token_data.usuario)
    if user is None:
        raise credentials_exception
    return user

#@app.get("/token/conductor", response_model = schema.ConductorFull)
#async def get_conductor_login(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
#    credentials_exception = HTTPException(
#        status_code=status.HTTP_401_UNAUTHORIZED,
#        detail="Credenciales erróneas",
#        headers={"WWW-Authenticate": "Bearer"},
#    )
#    try:
#        payload = crud.decodeToken(token)
#        cliente: str = payload.get("sub")
#        if cliente is None:
#            raise credentials_exception
#        token_data = schema.TokenData(cliente=cliente)
#    except JWTError:
#        raise credentials_exception
#    conductor = getConductorCorreo(db, correo=token_data.cliente)
#    if conductor is None:
#        raise credentials_exception
#    return conductor


async def get_cliente_activo(clienteActivo: schema.ClienteFull = Depends(get_usuario_login)):
    tokenData = schema.TokenData(usuario=clienteActivo.correo, rol="cliente")
    if tokenData is None:
        raise HTTPException(status_code=400, detail="Cliente inactivo")
    if not isinstance(clienteActivo, model.Cliente):
        raise HTTPException(status_code=400, detail="Cliente no identificado")
    return clienteActivo

async def get_conductor_activo(conductorActivo: schema.ConductorFull = Depends(get_usuario_login)):
    tokenData = schema.TokenData(usuario=conductorActivo.correo, rol="conductor")
    if tokenData is None:
        raise HTTPException(status_code=400, detail="Conductor inactivo")
    if not isinstance(conductorActivo, model.Conductor):
        raise HTTPException(status_code=400, detail="Conductor no identificado")
    return conductorActivo

async def get_administrador_activo(administradorActivo: schema.AdministradorFull = Depends(get_usuario_login)):
    tokenData = schema.TokenData(usuario=administradorActivo.correo, rol="administrador")
    if tokenData is None:
        raise HTTPException(status_code=400, detail="Administrador inactivo")
    if not isinstance(administradorActivo, model.Administrador):
        raise HTTPException(status_code=400, detail="Administrador no identificado")
    return administradorActivo


@app.post("/token", response_model=schema.Token)
async def comprobar_login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    usuario = crud.autenticarUsuario(db, form_data.username, form_data.password)
    if not usuario:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Correo o password incorrectos",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=TIEMPO_EXPIRACION)
    access_token = crud.crearToken(
        data={"sub": usuario.correo, "rol": usuario.rol}, expires_delta=access_token_expires
    )
    print("access_token: ", access_token)
    return {"access_token": access_token, "token_type": "bearer"}

@app.post("/administrador/nuevo", response_model=schema.AdministradorFull)
def crear_administrador(administrador: schema.AdministradorBase, db: Session = Depends(get_db)):
    if hasattr(administrador, "id"):
        db_administrador = crud.getAdministrador(db, id=administrador.id)
        if db_conductor:
            raise HTTPException(status_code=400, detail="Administrador ya creado")
    return crud.crearAdministrador(db=db, administrador=administrador)

@app.post("/cliente/nuevo", response_model=schema.ClienteFull)
def crear_cliente(cliente: schema.ClienteBase, db: Session = Depends(get_db)):
    db_cliente = crud.getClienteDNI(db, dni=cliente.dni)
    #if cliente.telefono is None: cliente.telefono = "666666666"
    print("Cliente telefono ", cliente.telefono)
    if db_cliente:
        raise HTTPException(status_code=400, detail="Cliente ya creado")
    if len(cliente.dni)!=9:
        raise HTTPException(status_code=400, detail="DNI erróneo")
    return crud.crearCliente(db=db, cliente=cliente)

@app.post("/conductor/nuevo", response_model=schema.ConductorFull)
def crear_conductor(conductor: schema.ConductorBase, db: Session = Depends(get_db)): # user: Session = Depends(get_conductor_activo), 
    print("Telefono Conductor 1 main ", conductor.telefono)
    if conductor.telefono is None: conductor.telefono = "666666666"
    print("Conductor main ", conductor)
    if hasattr(conductor, "id"):
        print("Conductor con id")
        db_conductor = crud.getConductor(db, id=conductor.id)
        if db_conductor:
            raise HTTPException(status_code=400, detail="Conductor ya creado")
    if len(conductor.dni)!=9:
        raise HTTPException(status_code=400, detail="DNI erróneo")  
    return crud.crearConductor(db=db, conductor=conductor)

@app.post("/autobus/nuevo", response_model=schema.AutobusFull)
def crear_autobus(autobus: schema.AutobusBase, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    if autobus.asientos <= 0:
        raise HTTPException(status_code=400, detail="Número de asientos menor o igual que 0")
    #if autobus.asientosLibres > autobus.asientos:
    #    raise HTTPException(status_code=400, detail="No puede haber más asientos libres que asientos")
    if hasattr(autobus, "id"):
        db_autobus = crud.getAutobus(db, id=autobus.id)
        if db_autobus:
            raise HTTPException(status_code=400, detail="Autobus ya creado")
    return crud.crearAutobus(db=db, autobus=autobus)

@app.post("/viaje/nuevo", response_model=schema.ViajeFull)
def crear_viaje(viaje: schema.ViajeBase, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    autobus = crud.getAutobus(db=db, idAutobus=viaje.autobus)
    if autobus.asientosLibres==0:
        raise HTTPException(status_code=400, detail="Autobus completo")
    if hasattr(viaje, "id"):
        db_viaje = crud.getViaje(db, id=viaje.id)
        #crud.updateAutobus(db=db, newModelo=autobus.modelo, newAsientosLibres=autobus.asientosLibres)
        if db_viaje:
            raise HTTPException(status_code=400, detail="Viaje ya creado")
    return crud.crearViaje(db=db, viaje=viaje)

@app.post("/billete/nuevo", response_model=schema.BilleteFull)
def crear_billete(billete: schema.BilleteBase, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    if billete.cliente!=user.id:
        raise HTTPException(status_code=401, detail="Perfil no autorizado")
    if hasattr(billete, "id"):
        db_billete = crud.getBillete(db, id=billete.id)
        if db_billete:
            raise HTTPException(status_code=400, detail="Billete ya creado")
        if crud.comprobarAsiento(db, billete.id, billete.asiento) is False:
            raise HTTPException(status_code=400, detail="Asiento no disponible")
        if crud.comprobarFechas(db, billete.fechaSalida, billete.fechaLlegada) is False:
            raise HTTPException(status_code=400, detail="Fechas erróneas")
    return crud.crearBillete(db=db, billete=billete)

@app.post("/ruta/nueva", response_model=schema.RutaFull)
def crear_ruta(ruta: schema.RutaBase, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    if hasattr(ruta, "id"):
        db_ruta = crud.getRuta(db, id=ruta.id)
        if db_ruta:
            raise HTTPException(status_code=400, detail="Ruta ya creada")
    return crud.crearRuta(db=db, ruta=ruta)

@app.get("/clientes/{idCliente}/perfil", response_model=schema.ClienteFull)
def get_cliente(idCliente, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    if idCliente!=user.id:
        raise HTTPException(status_code=401, detail="Perfil no autorizado")
    db_cliente = crud.getCliente(db, idCliente=idCliente)
    if db_cliente is None:
        raise HTTPException(status_code=404, detail="Cliente no encontrado")
    return db_cliente

@app.get("/conductores/{idConductor}/perfil", response_model=schema.ConductorFull)
def get_conductor(idConductor, user: Session = Depends(get_conductor_activo), db: Session = Depends(get_db)):
    if idConductor!=user.id:
        raise HTTPException(status_code=401, detail="Perfil no autorizado")
    db_conductor = crud.getConductor(db, idConductor=idConductor)
    if db_conductor is None:
        raise HTTPException(status_code=404, detail="Conductor no encontrado")
    return db_conductor

@app.get("/clientes/", response_model=List[schema.ClienteFull])
def get_clientes(skip: int = 0, limit: int = 100, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    clientes = crud.getClientes(db, skip=skip, limit=limit)
    return clientes

@app.get("/conductores/", response_model=List[schema.ConductorFull])
def get_conductores(skip: int = 0, limit: int = 100, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    conductores = crud.getConductores(db, skip=skip, limit=limit)
    return conductores

@app.get("/autobuses/", response_model=List[schema.AutobusFull])
def get_autobuses(skip: int = 0, limit: int = 100, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    autobuses = crud.getAutobuses(db, skip=skip, limit=limit)
    return autobuses

@app.get("/viajes/", response_model=List[schema.ViajeFull])
def get_viajes(skip: int = 0, limit: int = 100, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    viajes = crud.getViajes(db, skip=skip, limit=limit)
    return viajes

@app.get("/billetes/", response_model=List[schema.BilleteFull])
def get_billetes(skip: int = 0, limit: int = 100, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    billetes = crud.getBilletes(db, skip=skip, limit=limit)
    return billetes

@app.get("/rutas/{idRuta}", response_model=schema.RutaFull)
def get_ruta(idRuta, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    db_ruta = crud.getRuta(db, idRuta)
    if db_ruta is None:
        raise HTTPException(status_code=404, detail="Ruta no encontrada")
    return db_ruta

@app.get("/viaje/{idViaje}/billetes/", response_model=List[schema.BilleteFull])
def get_asientos_ocupados(idViaje, skip: int = 0, limit: int = 100, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    return crud.getAsientosOcupados(db=db, idViaje=idViaje, skip=skip, limit=limit)

@app.get("/cliente/{idCliente}/billetes/", response_model=List[schema.BilleteFull])
def get_billetes_cliente(idCliente, skip: int = 0, limit: int = 100, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    if idCliente!=user.id:
        raise HTTPException(status_code=401, detail="Acceso no autorizado")
    return crud.getBilletesCliente(db=db, idCliente=idCliente, skip=skip, limit=limit)

@app.get("/conductor/{idConductor}/viajes/", response_model=List[schema.ViajeFull])
def get_viajes_conductor(idConductor, skip: int = 0, limit = 100, user: Session = Depends(get_conductor_activo), db: Session = Depends(get_db)):
    if idConductor!=user.id:
        raise HTTPException(status_code=401, detail="Acceso no autorizado")
    return crud.getViajesConductor(db=db, idConductor=idConductor, skip=skip, limit=limit)

@app.get("/cliente/billete/{idBillete}/duracion")
def get_duracion(idBillete, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    billete = crud.getBillete(db=db, idBillete=idBillete)
    if billete.cliente!=user.id:
        raise HTTPException(status_code=401, detail="Acceso no autorizado")
    if billete is None:
        raise HTTPException(status_code=404, detail="Billete no encontrado")
    cliente = crud.getCliente(db, billete.cliente)
    if cliente.correo != user.username:
        raise HTTPException(status_code=400, detail="El billete no pertenece al cliente")
    return crud.getDuracion(db=db, idCliente=cliente.id, idBillete=idBillete)

@app.get("/viajes/origen/{origen}/destino/{destino}", response_model=List[schema.ViajeFull])
def get_viajes_origen_destino(origen, destino, skip: int = 0, limit: int = 100, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    if origen is None:
        raise HTTPException(status_code=400, detail="Origen erróneo")
    if destino is None:
        raise HTTPException(status_code=400, detail="Destino erróneo")
    return crud.getViajesOrigenDestino(db, origen, destino, skip, limit)



@app.delete("/cliente/{idCliente}/borrar")
def delete_cliente(idCliente, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    if idCliente!=user.id:
        raise HTTPException(status_code=401, detail="Acceso no autorizado")
    crud.deleteCliente(db, idCliente)

@app.delete("/conductor/{idConductor}/borrar")
def delete_conductor(idConductor, user: Session = Depends(get_conductor_activo), db: Session = Depends(get_db)):
    if idConductor!=user.id:
        raise HTTPException(status_code=401, detail="Acceso no autorizado")
    crud.deleteConductor(db, idConductor)

@app.delete("/autobus/{idAutobus}/borrar")
def delete_autobus(idAutobus, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    crud.deleteAutobus(db, idAutobus)

@app.delete("/viaje/{idViaje}/borrar")
def delete_viaje(idViaje, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    crud.deleteViaje(db, idViaje)

@app.delete("/billete/{idBillete}/borrar")
def delete_billete(idBillete, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    if billete.cliente!=user.id:
        raise HTTPException(status_code=401, detail="Acceso no autorizado")
    crud.deleteBillete(db, idBillete)

@app.delete("/ruta/{idRuta}/borrar")
def delete_ruta(idRuta, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    crud.deleteRuta(db, idRuta)



@app.patch("/administrador/{idAdministrador}/editar", response_model=schema.AdministradorFull)
def set_administrador(idAdministrador, newNombre: Optional[str] = None, newCorreo: Optional[str] = None, newPassword: Optional[str] = None, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    if idAdministrador!=user.id:
        raise HTTPException(status_code=401, detail="Acceso no autorizado")
    db_administrador = crud.getAdministrador(db, idAdministrador=idAdministrador)
    if db_administrador is None:
        raise HTTPException(status_code=404, detail="Administrador no encontrado")
    return crud.updateAdministrador(db, idAdministrador, newNombre, newCorreo, newPassword)

@app.patch("/clientes/{idCliente}/editar", response_model=schema.ClienteFull)
def set_cliente(idCliente, newNombre: Optional[str] = None, newApellidos: Optional[str] = None, newDNI: Optional[str] = None, newPassword: Optional[str] = None, newTelefono: Optional[str] = None, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    if idCliente!=user.id:
        raise HTTPException(status_code=401, detail="Acceso no autorizado")
    db_cliente = crud.getCliente(db, idCliente=idCliente)
    print("New nombre ", newNombre)
    print("New apellidos ", newApellidos)
    if db_cliente is None:
        raise HTTPException(status_code=404, detail="Cliente no encontrado")
    if newDNI is not None:
        print("len DNI ", len(newDNI))
        if len(newDNI) != 9:
            raise HTTPException(status_code=400, detail="DNI incorrecto")
    #if newPassword is None:
    #    newPassword = ""
    return crud.updateCliente(db, idCliente, newNombre, newApellidos, newDNI, newPassword, newTelefono)


@app.patch("/autobuses/{idAutobus}/editar", response_model=schema.AutobusFull)
def set_autobus(idAutobus, newModelo: Optional[str] = None, newAsientos: Optional[int] = None, newAsientosLibres: Optional[int] = None, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    db_autobus = crud.getAutobus(db, idAutobus=idAutobus)
    if newAsientos<=0 or newAsientosLibres<=0:
        raise HTTPException(status_code=400, detail="Número de asientos erróneo")
    if newAsientosLibes > newAsientos:
        raise HTTPException(status_code=400, detail="No puede haber más asientos libres que asientos")
    if db_autobus is None:
        raise HTTPException(status_code=404, detail="Autobus no encontrado")
    return crud.updateAutobus(db, idAutobus, newModelo, newAsientos, newAsientosLibres)


@app.patch("/conductores/{idConductor}/editar", response_model=schema.ConductorFull)
def set_conductor(idConductor, newNombre: Optional[str] = None, newApellidos: Optional[str] = None, newDNI: Optional[str] = None, newPassword: Optional[str] = None, newTelefono: Optional[str] = None, user: Session = Depends(get_conductor_activo), db: Session = Depends(get_db)):
    if idConductor!=user.id:
        raise HTTPException(status_code=401, detail="Acceso no autorizado")
    db_conductor = crud.getConductor(db, idConductor=idConductor)
    if db_conductor is None:
        raise HTTPException(status_code=404, detail="Conductor no encontrado")
    return crud.updateConductor(db, idConductor, newNombre, newApellidos, newDNI, newPassword, newTelefono)


@app.patch("/rutas/{idRuta}/editar", response_model=schema.RutaFull)
def set_ruta(idRuta, newCiudades: Optional[str] = None, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    db_ruta = crud.getRuta(db, idRuta=idRuta)
    if db_ruta is None:
        raise HTTPException(status_code=404, detail="Ruta no encontrada")
    return crud.updateRuta(db, idRuta, newCiudades)


@app.patch("/viajes/{idViaje}/editar", response_model=schema.ViajeFull)
def set_viaje(idViaje, newConductor: Optional[int] = None, newAutobus: Optional[int] = None, newRuta: Optional[int] = None, newPrecio: Optional[float] = None, user: Session = Depends(get_administrador_activo), db: Session = Depends(get_db)):
    db_viaje = crud.getViaje(db, idViaje=idViaje)
    if db_viaje is None:
        raise HTTPException(status_code=404, detail="Viaje no encontrado") # CAMBIAR RELACIONES ENTRE RUTAS Y LAS OTRAS TABLAS (HECHO)
    return crud.updateViaje(db, idViaje, newConductor, newAutobus, newRuta, newPrecio) # MIRAR DOCUMENTACION FASTAPI PARA AUTENTICACION (HAY QUE PROBARLO)

#@app.patch("/viajes/{idViaje}", response_model=schema.ViajeFull) # HACER MAS FUNCIONES
#def set_viaje_autobus(idViaje, newAutobus: int, db: Session = Depends(get_db)): # FUNCIONES TEST
#    db_viaje = crud.getViaje(db, idViaje=idViaje)
#    if db_viaje is None:
#        raise HTTPException(status_code=404, detail="Viaje no encontrado") 
#    return crud.updateViajeAutobus(db, idViaje, newAutobus)

#@app.patch("/viajes/{idViaje}", response_model=schema.ViajeFull)
#def set_viaje_ruta(idViaje, newRuta: int, db: Session = Depends(get_db)): # SE TESTEAN LOS ENDPOINTS DE LA API
#    db_viaje = crud.getViaje(db, idViaje=idViaje)
#    if db_viaje is None:
#        raise HTTPException(status_code=404, detail="Viaje no encontrado")
#    return crud.updateViajeRuta(db, idViaje, newRuta)

#@app.patch("/viajes/{idViaje}", response_model=schema.ViajeFull)
#def set_viaje_precio(idViaje, newPrecio: float, db: Session = Depends(get_db)):
#    db_viaje = crud.getViaje(db, idViaje=idViaje)
#    if db_viaje is None:
#        raise HTTPException(status_code=404, detail="Viaje no encontrado")
#    return crud.updateViajePrecio(db, idViaje, newPrecio)

@app.patch("/billetes/{idBillete}/editar", response_model=schema.BilleteFull) # REVISAR
def set_billete(idBillete, newFechaSalida: Optional[datetime] = None, newFechaLlegada: Optional[datetime] = None, newAsiento: Optional[int] = None, user: Session = Depends(get_cliente_activo), db: Session = Depends(get_db)):
    db_billete = crud.getBillete(db, idBillete=idBillete)
    if db_billete.cliente!=user.id:
        raise HTTPException(status_code=401, detail="Acceso no autorizado")
    if crud.comprobarAsiento(db, idBillete, billete.asiento) is False:
        raise HTTPException(status_code=404, detail="Asiento no disponible")
    if crud.comprobarFechas(db, billete.fechaSalida, billete.fechaLlegada) is False:
            raise HTTPException(status_code=400, detail="Fechas erróneas")
    if db_billete is None:
        raise HTTPException(status_code=404, detail="Billete no encontrado")
    return crud.updateBillete(db, idBillete, newFechaSalida, newFechaLlegada, newAsiento)

#@app.put("/rutas/{id_ruta}", response_model=schema.RutaFull)
#def set_ruta_horas(idRuta, newHoraSalida: str, newHoraLlegada: str, db: Session = Depends(get_db)):
#    db_ruta = crud.getRuta(db, idRuta=idRuta)
#    if db_ruta is None:
#        raise HTTPException(status_code=404, detail="Ruta no encontrada")
#    return crud.updateRutaHoras(db, idRuta, newHoraSalida, newHoraLlegada)






