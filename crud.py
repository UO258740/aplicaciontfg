from sqlalchemy.orm import Session
#import model, schema
#import model
#import schema

#import model, schema
from . import model
from . import schema

from passlib.context import CryptContext
from jose import JWTError, jwt
from typing import Optional
from datetime import datetime, timedelta

CLAVE_SECRETA = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITMO = "HS256"


def getAdministrador(db: Session, idAdministrador: int):
    return db.query(model.Administrador).filter(model.Administrador.id == idAdministrador).first()

def getCliente(db: Session, idCliente: int):
    return db.query(model.Cliente).filter(model.Cliente.id == idCliente).first()

def getConductor(db: Session, idConductor: int):
    return db.query(model.Conductor).filter(model.Conductor.id == idConductor).first()

def getAutobus(db: Session, idAutobus: int):
    return db.query(model.Autobus).filter(model.Autobus.id == idAutobus).first()

def getBillete(db: Session, idBillete: int):
    return db.query(model.Billete).filter(model.Billete.id == idBillete).first()

def getViaje(db: Session, idViaje: int):
    return db.query(model.Viaje).filter(model.Viaje.id == idViaje).first()

def getRuta(db: Session, idRuta: int):
    return db.query(model.Ruta).filter(model.Ruta.id == idRuta).first()

def getClienteDNI(db: Session, dni: str):
    return db.query(model.Cliente).filter(model.Cliente.dni == dni).first()

def getConductorDNI(db: Session, dni: str):
    return db.query(model.Conductor).filter(model.Conductor.dni == dni).first()

def getClienteCorreo(db: Session, correo: str):
    return db.query(model.Cliente).filter(model.Cliente.correo == correo).first()

def getConductorCorreo(db: Session, correo: str):
    return db.query(model.Conductor).filter(model.Conductor.correo == correo).first()

def getAdministradorCorreo(db: Session, correo: str):
    return db.query(model.Administrador).filter(model.Administrador.correo == correo).first()

def getUserCorreo(db: Session, correo: str):
    return db.query(model.Usuario).filter(model.Usuario.correo == correo).first()



pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def verificarPassword(password, hash):
    return pwd_context.verify(password, hash)

def getPasswordHash(password):
    return pwd_context.hash(password)

def autenticarUsuario(db: Session, correo: str, password: str):
    usuario = getUserCorreo(db, correo)
    #if not cliente:
    #    cliente = getConductorCorreo(db, correo)
    #    if not cliente:
    #        cliente = getAdministradorCorreo(db, correo)
    #        if not cliente:
    #            return False 
    #print("PASSWORD: ", password)
    #print("HASH: ", cliente.password)
    if not usuario:
        return False
    if not verificarPassword(password, usuario.password):
        return False
    return usuario

def crearToken(data: dict, expires_delta: Optional[timedelta] = None):
    codificar = data.copy()
    #print("cliente: ", cliente)
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    #codificar.update({"exp": expire})
    token = jwt.encode(codificar, CLAVE_SECRETA, algorithm=ALGORITMO)
    return token

def decodeToken(token: str):
    return jwt.decode(token, CLAVE_SECRETA, algorithms=[ALGORITMO])

def getClienteLogin(db: Session, token: str):
    payload = jwt.decode(token, CLAVE_SECRETA, algorithms=[ALGORITMO])
    cliente: str = payload.get("sub")
    if cliente is None:
        raise credentials_exception
    token_data = schema.TokenData(cliente=cliente)
    return token_data
    


def getClientes(db: Session, skip: int = 0, limit: int = 100):
    return db.query(model.Cliente).offset(skip).limit(limit).all()

def getConductores(db: Session, skip: int = 0, limit: int = 100):
    return db.query(model.Conductor).offset(skip).limit(limit).all()

def getAutobuses(db: Session, skip: int = 0, limit: int = 100):
    return db.query(model.Autobus).offset(skip).limit(limit).all()

def getBilletes(db: Session, skip: int = 0, limit: int = 100):
    return db.query(model.Billete).offset(skip).limit(limit).all()

def getViajes(db: Session, skip: int = 0, limit: int = 100):
    return db.query(model.Viaje).offset(skip).limit(limit).all()

def getBilletesCliente(db:Session, idCliente: int, skip: int, limit: int):
    return db.query(model.Billete).filter(model.Billete.cliente == idCliente).offset(skip).limit(limit).all()

def getAsientosOcupados(db: Session, idViaje: int, skip: int, limit: int):
    return db.query(model.Billete).filter(model.Billete.viaje == idViaje).offset(skip).limit(limit).all()

def getViajesConductor(db: Session, idConductor: int, skip: int, limit: int):
    return db.query(model.Viaje).filter(model.Viaje.conductor == idConductor).offset(skip).limit(limit).all()

def getDuracion(db: Session, idBillete: int):
    billete = getBillete(db, idBillete)
    duracion = billete.fechaLlegada - billete.fechaSalida
    return duracion

def getViajesOrigenDestino(db:Session, origen: str, destino: str, skip: int, limit: int):
    rutas = getRutasOrigenDestino(db, origen, destino, skip, limit)
    print("Rutas ", rutas)
    lista = []
    i = 0
    for ruta in rutas:
        print("Ruta ", ruta)
        lista.append(db.query(model.Viaje).filter(model.Viaje.ruta == ruta).first())
        print("Lista ", lista)
        i=i+1
    for viaje in lista:
        print("Viaje ", viaje)
    return lista

def getRutasOrigenDestino(db: Session, origen: str, destino: str, skip: int, limit: int):
    listaRutas = db.query(model.Ruta.ciudades).offset(skip).limit(limit).all()
    print("Lista Rutas 1 ", listaRutas)
    checkOrigen = False
    checkDestino = False
    rutas = []

    k = 0
    #while k<len(listaRutas):
        #ciudades = listaRutas.popLeft()
        #print("Ciudades ", ciudades, " len ciudades ", len(ciudades))
        #ciudades2 = ciudades[0].split()
        #print("Ciudades split ", ciudades2)
        #print("Ciudades primera ", ciudades2[0])
        #for ciudad in ciudades2:
        #    if ciudad==origen: 
        #        print("TRUE ", ciudad)
        #        print(origen)
        #        checkOrigen = True
        #    if ciudad==destino and checkOrigen: 
        #        print("TRUE ", ciudad)
        #        print(destino)
        #        checkDestino = True
        #k = k+1
        #if checkOrigen and checkDestino: rutas.append(k)
        #print("Rutas ", rutas)

    index = 1
    for ruta in listaRutas:
        print("Lista Rutas 2 ", ruta)
        ciudades = ruta[0].split()
        for ciudad in ciudades:
            if ciudad==origen: 
                print("TRUE ", ciudad)
                print(origen)
                checkOrigen = True
            if ciudad==destino and checkOrigen: 
                print("TRUE ", ciudad)
                print(destino)
                checkDestino = True
        if checkOrigen and checkDestino: rutas.append(index)
        index = index+1
    return rutas

def crearAdministrador(db: Session, administrador: schema.AdministradorBase):
    administrador.password = getPasswordHash(administrador.password)
    db_administrador = model.Administrador(**administrador.dict())
    db.add(db_administrador)
    db.commit()
    db.refresh(db_administrador)
    return db_administrador

def crearCliente(db: Session, cliente: schema.ClienteBase):
    #fake_hashed_password = cliente.password + "notreallyhashed"
    cliente.password = getPasswordHash(cliente.password)
    db_cliente = model.Cliente(**cliente.dict())
    db_cliente.puntos = 0
    db_cliente.rol = 'cliente'
    #db_cliente = model.Cliente(nombre=cliente.nombre, apellidos=cliente.apellidos, dni=cliente.dni, password=fake_hashed_password,telefono=cliente.telefono, puntos=0)
    db.add(db_cliente)
    db.commit()
    db.refresh(db_cliente)
    return db_cliente

def crearConductor(db: Session, conductor: schema.ConductorBase):
    #fake_hashed_password = conductor.password + "notreallyhashed"
    print("Conductor Telefono CRUD ", conductor.telefono)
    conductor.password = getPasswordHash(conductor.password)
    db_conductor = model.Conductor(**conductor.dict())
    db_conductor.rol = 'conductor'
    #db_conductor = model.Conductor(nombre=conductor.nombre, apellidos=conductor.apellidos, dni=conductor.dni, password=fake_hashed_password, telefono=conductor.telefono)
    db.add(db_conductor)
    db.commit()
    db.refresh(db_conductor)
    return db_conductor

def crearAutobus(db: Session, autobus: schema.AutobusBase):
    db_autobus = model.Autobus(**autobus.dict())
    db_autobus.asientosLibres = autobus.asientos
    #db_autobus = model.Autobus(modelo=autobus.modelo, asientosLibres=autobus.asientosLibres)
    db.add(db_autobus)
    db.commit()
    db.refresh(db_autobus)
    return db_autobus

def crearViaje(db: Session, viaje: schema.ViajeBase):
    db_viaje = model.Viaje(**viaje.dict())
    #db_ruta = model.Ruta(conductor=ruta.conductor, autobus=ruta.autobus, itinerario=ruta.itinerario,  
    #                            fechaSalida=ruta.fechaSalida, fechaLlegada=ruta.fechaLlegada, duracion=ruta.duracion, precio=ruta.precio)
    db.add(db_viaje)
    db.commit()
    db.refresh(db_viaje)
    return db_viaje

def crearBillete(db: Session, billete: schema.BilleteBase):
    db_billete = model.Billete(**billete.dict())
    #db_viaje = model.Viaje(cliente=viaje.cliente, ruta=viaje.ruta, asiento=viaje.asiento)
    updateClientePuntos(db, billete.cliente, 100)
    db_viaje = getViaje(db, db_billete.viaje)
    updateAutobusAsientos(db, db_viaje.autobus)
    db.add(db_billete)
    db.commit()
    db.refresh(db_billete)
    return db_billete

def crearRuta(db: Session, ruta: schema.RutaBase):
    db_ruta = model.Ruta(**ruta.dict())
    ciudadesRuta = ruta.ciudades.split()
    numeroCiudadesRuta = 0
    for i in range(len(ciudadesRuta)):
        numeroCiudadesRuta = numeroCiudadesRuta+1
    db_ruta.numeroCiudades = numeroCiudadesRuta
    #db_itinerario = model.Itinerario(numeroCiudades=itinerario.numeroCiudades, ciudades=itinerario.ciudades)
    db.add(db_ruta)
    db.commit()
    db.refresh(db_ruta)
    return db_ruta


def updateAdministrador(db: Session, idAdministrador: int, newNombre: str, newCorreo: str, newPassword: str):
    administrador = getAdministrador(db, idAdministrador)
    if newNombre is not None: administrador.nombre = newNombre
    if newCorreo is not None: administrador.correo = newCorreo
    if newPassword is not None: administrador.password = getPasswordHash(newPassword)
    db.commit()
    db.refresh(administrador)
    return administrador

def updateClientePuntos(db: Session, idCliente: int, newPuntos: int):
    cliente = getCliente(db, idCliente)
    cliente.puntos = newPuntos
    db.commit()
    db.refresh(cliente)
    return cliente

def updateCliente(db: Session, idCliente: int, newNombre: str, newApellidos: str, newDNI: str, newPassword: str, newTelefono: str):
    cliente = getCliente(db, idCliente)
    if newNombre is not None: cliente.nombre = newNombre
    if newApellidos is not None: cliente.apellidos = newApellidos
    if newDNI is not None: cliente = dni = newDNI
    if newPassword is not None: cliente.password = getPasswordHash(newPassword)
    if newTelefono is not None: cliente.telefono = newTelefono
    db.commit()
    db.refresh(cliente)
    return cliente


def updateConductor(db: Session, idConductor: int, newNombre: str, newApellidos: str, newDNI: str, newPassword: str, newTelefono: str):
    conductor = getConductor(db, idConductor)
    if newNombre is not None: conductor.nombre = newNombre
    if newApellidos is not None: conductor.apellidos = newApellidos
    if newDNI is not None: conductor.dni = newDNI
    if newPassword is not None: conductor.password = getPasswordHash(newPassword)
    if newTelefono is not None: conductor.telefono = newTelefono
    db.commit()
    db.refresh(conductor)
    return conductor

def updateAutobusAsientos(db: Session, idAutobus: int):
    autobus = getAutobus(db, idAutobus)
    if autobus.asientosLibres>0: autobus.asientosLibres = autobus.asientosLibres-1
    db.commit()
    db.refresh(autobus)

def updateAutobus(db: Session, idAutobus: int, newModelo: str, newAsientos: int, newAsientosLibres: int):
    autobus = getAutobus(db, idAutobus)
    if newModelo is not None: autobus.modelo = newModelo
    if newAsientos is not None: autobus.asientos = newAsientos
    if newAsientosLibres is not None: autobus.asientosLibres = newAsientosLibres
    db.commit()
    db.refresh(autobus)
    return autobus


def updateRuta(db: Session, idRuta: int, newCiudades: str):
    ruta = getRuta(db, idRuta)
    if newCiudades is not None:
        ciudadesRuta = newCiudades.split()
        numeroCiudadesRuta = 0
        for i in range(len(ciudadesRuta)):
            numeroCiudadesRuta = numeroCiudadesRuta+1
        ruta.numeroCiudades = numeroCiudadesRuta
        ruta.ciudades = newCiudades
        db.commit()
        db.refresh(ruta)
    return ruta

def updateViaje(db: Session, idViaje: int, newConductor: int, newAutobus: int, newRuta: int, newPrecio: int):
    viaje = getViaje(db, idViaje)
    if newConductor is not None: viaje.conductor = newConductor
    if newAutobus is not None: viaje.autobus = newAutobus
    if newRuta is not None: viaje.ruta = newRuta
    if newPrecio is not None: viaje.precio = newPrecio
    db.commit()
    db.refresh(viaje)
    return viaje

#def updateViajeAutobus(db: Session, idViaje: int, newAutobus: int):
#    viaje = getViaje(db, idViaje)
#    viaje.autobus = newAutobus
#    db.commit()
#    db.refresh(viaje)
#    return viaje

#def updateViajeRuta(db: Session, idViaje: int, newRuta: int):
#    viaje = getViaje(db, idViaje)
#    viaje.ruta = newRuta
#    db.commit()
#    db.refresh(viaje)
#    return viaje

#def updateViajePrecio(db: Session, idViaje: int, newPrecio: int):
#    viaje = getViaje(db, idViaje)
#    viaje.precio = newPrecio
#    db.commit()
#    db.refresh(viaje)
#    return viaje

#def updateRutaHoras(db: Session, idRuta: int, newHoraSalida: int, newHoraLlegada: int):
#    ruta = getRuta(db, idRuta)
#    ruta.horaSalida = newHoraSalida
#    ruta.horaLlegada = newHoraLlegada
#    db.commit()
#    db.refresh(ruta)
#    return True

def updateBillete(db: Session, idBillete: int, newFechaSalida: str, newFechaLlegada: str, newAsiento: int):
    billete = getBillete(db, idBillete)
    if newFechaSalida is not None: billete.fechaSalida = newFechaSalida
    if newFechaLlegada is not None: billete.fechaLlegada = newFechaLlegada
    if newAsiento is not None: billete.asiento = newAsiento
    db.commit()
    db.refresh(billete)
    return billete

def comprobarAsiento(db: Session, idBillete: int, asiento: int):
    billete = getBillete(db, idBillete)
    asientoOcupado = db.query(model.Billete).filter(model.Billete.viaje == billete.viaje).filter(model.Billete.asiento == asiento).first()
    if asientoOcupado is None: return True
    else: return False

def comprobarFechas(db: Session, newFechaSalida: str, newFechaLlegada: str):
    fechaSalida = datetime.strptime(newFechaSalida, "%y-%m-%dT%H:%M:%SD")
    fechaLlegada = datetime.strptime(newFechaLlegada, "%y-%m-%dT%H:%M:%SD")
    if fechaSalida < datetime.now() or fechaLlegada < datetime.now() or fechaSalida < fechaLlegada:
        return False
    else:
        return True


def deleteCliente(db: Session, idCliente: int):
    cliente = getCliente(db, idCliente)
    db.delete(cliente)
    db.commit()
    #return True

def deleteConductor(db: Session, idConductor: int):
    conductor = getConductor(db, idConductor)
    db.delete(conductor)
    db.commit()
    #return True

def deleteAutobus(db: Session, idAutobus: int):
    autobus = getAutobus(db, idAutobus)
    db.delete(autobus)
    db.commit()

def deleteViaje(db: Session, idViaje: int):
    viaje = getViaje(db, idViaje)
    db.delete(viaje)
    db.commit()
    #return True

def deleteBillete(db: Session, idBillete: int):
    billete = getBillete(db, idBillete)
    viaje = getViaje(db, billete.viaje)
    autobus = getAutobus(db, viaje.autobus)
    autobus.asientosLibres = autobus.asientos 
    db.delete(billete)
    db.commit()

def deleteRuta(db: Session, idRuta: int):
    ruta = getRuta(db, idRuta)
    db.delete(ruta)
    db.commit()