import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from fastapi.testclient import TestClient

from ..main import app, get_db
from ..database import Base
from .. import model

# SQLALCHEMY_DATABASE_URL = "mysql+pymysql://uo258740:rom31abi@localhost:3306/dbPruebas"
SQLALCHEMY_DATABASE_URL = "sqlite:///./test/sql_app.db"
# SQLALCHEMY_DATABASE_URL = "sqlite:///:memory:"
CONNECT_ARGS = {"check_same_thread": False}

@pytest.fixture(scope='module')
def setup_client():
    """Esta función prepara una base de datos para test, arranca la aplicación y crea un cliente
    para conectar con esa aplicación/base de datos.
    
    Todos los test que vayan a testear la API Rest y por tanto necesiten este cliente
    deben declarar esta función como parámetro. Por ejemplo:
    
    def test_comprar_billetes(setup_client):
        ...

    Pytest se asegurará de que esta función esté ejecutada antes de lanzar ese test y
    pasará como parámetro al test el resultado de esta función (el client), por tanto
    la función puede comenzar haciendo:

        client = setup_client
    """
    print("Fixture invocado")
    engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args=CONNECT_ARGS)
    TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    model.Base.metadata.create_all(bind=engine)

    def override_get_db():
        try:
            db = TestingSessionLocal()
            yield db
        finally:
            db.close()

    app.dependency_overrides[get_db] = override_get_db
    client = TestClient(app)

    # Aqui debería inicializarse la base de datos con datos de ejemplo, 
    # de lo contrario estará vacía
    return client
