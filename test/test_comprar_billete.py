#import sys,os
#sys.path.append(os.path.join(os.path.dirname(__file__),os.pardir,"modeloDatos"))
#import myapplib

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import timedelta
import pytest

#from .. import database, main, crud
from ..database import Base
from ..main import app, get_db
from ..crud import getPasswordHash, autenticarUsuario, crearToken
#import database.Base
#import main.app
#import main.get_db
#from .. import database, main
from .aux_func import setup_client

#import unittest

# Datos de los usuarios sobre los que se realizan las pruebas
cliente_ejemplo = {"nombre": "ClienteBillete", "apellidos": "Cliente Billete", "dni": "11223344A", "correo": "clienteBillete@email.com", 
            "password": "billete", "telefono": "666666666", "rol": "cliente"} #666333999

admin_ejemplo = {"nombre": "AdminBillete", "apellidos": "Admin Billete", "dni": "11223344B", "correo": "adminBillete@email.com", 
            "password": "billete", "telefono": "666666666", "rol": "administrador"}

conductor_ejemplo = {"nombre": "ConductorBillete", "apellidos": "Conductor Billete", "dni": "11223344C", "correo": "conductorBillete@email.com", 
            "password": "billete", "telefono": "666666666", "rol": "conductor"}

autobus_ejemplo = {"modelo": "autobusBillete", "asientos": 45}

autobus_ejemplo_lleno = {"modelo": "autobusLleno", "asientos": 1}

ruta_ejemplo = {"ciudades": "Salamanca Zamora Leon"}

viaje_ejemplo = {"conductor": 1, "autobus": 1, "ruta": 1, "precio": 11.60}

# Billete que el usuario va a comprar
billete_ejemplo = {"cliente": -1, "viaje": 1, "fechaSalida": "2022-05-08T10:30:00", 
        "fechaLlegada": "2022-05-08T14:30:00", "asiento": 20}

# id que el usuario va a tener una vez se haya creado
id_cliente = "-1"

# Campos del usuario que se han de verificar
campos_verificables = ["nombre", "apellidos", "dni", "correo", "telefono", "rol"]

# Campos del billete que se han de verificar
campos_billete = ["cliente", "viaje", "fechaSalida", "fechaLlegada", "asiento"]

# Función que verifica si dos JSON son iguales en los campos especificados
# saltándose los que se especifiquen en exclude
def verificar_igualdad(user1, user2, campos=campos_verificables, exclude=[]):
    for k in campos:
        if k in exclude:
            continue
        assert user1[k] == user2[k]



def testCrearAdministrador(setup_client):
    client=setup_client
    response = client.post(
        "/administrador/nuevo",
        #headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        json=admin_ejemplo
    )
    print(response.json())
    assert response.status_code == 200 or response.status_code == 400
    verificar_igualdad(response.json(), admin_ejemplo, exclude=["telefono"])

def testCrearConductor(setup_client):
    client=setup_client
    response = client.post(
        "/conductor/nuevo",
        #headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        json=conductor_ejemplo
    )
    print(response.json())
    assert response.status_code == 200 or response.status_code == 400
    verificar_igualdad(response.json(), conductor_ejemplo, exclude=["telefono"])

def testLoginAdmin(setup_client):
    client=setup_client
    response = client.post(
        "/token",
        headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        data={"grant_type": "", "username": admin_ejemplo["correo"], "password": admin_ejemplo["password"], "scope": "", 
            "client_id": "", "client_secret": ""}
    )
    print(response.json())
    response_header = {"Authorization": "Bearer "+response.json()["access_token"], "token_type": "bearer"}

def testCrearAutobus(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": admin_ejemplo["rol"]}, expires_delta=access_token_expires)

    response = client.post(
        "/autobus/nuevo",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json=autobus_ejemplo
    )
    print("Autobus ", response.json())
    assert response.status_code == 200 or response.status_code ==400
    verificar_igualdad(response.json(), autobus_ejemplo, campos=["modelo", "asientos"])

def testCrearAutobusLleno(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": admin_ejemplo["rol"]}, expires_delta=access_token_expires)

    response = client.post(
        "/autobus/nuevo",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json=autobus_ejemplo_lleno
    )
    assert response.status_code == 200 or response.status_code == 400
    verificar_igualdad(response.json(), autobus_ejemplo_lleno, campos=["modelo", "asientos"])

def testCrearRuta(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": admin_ejemplo["rol"]}, expires_delta=access_token_expires)

    response = client.post(
        "/ruta/nueva",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json=ruta_ejemplo
    )
    assert response.status_code == 200 or response.status_code ==400
    verificar_igualdad(response.json(), ruta_ejemplo, campos=["ciudades"])
    assert response.json()["numeroCiudades"] == 3

def testCrearViaje(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": admin_ejemplo["rol"]}, expires_delta=access_token_expires)

    response = client.post(
        "/viaje/nuevo",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json=viaje_ejemplo
    )
    print("Viaje ", response.json())
    assert response.status_code == 200 or response.status_code ==400
    verificar_igualdad(response.json(), viaje_ejemplo, campos=["autobus", "conductor", "ruta", "precio"])
    

def testCrearCliente(setup_client):
    client=setup_client
    response = client.post(
        "/cliente/nuevo",
        #headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        json=cliente_ejemplo
    )
    print(response.json())
    billete_ejemplo["cliente"] = response.json()["id"]
    id_cliente = billete_ejemplo["cliente"]
    print("id Cliente ", id_cliente)
    assert response.status_code == 200 or response.status_code == 400
    verificar_igualdad(response.json(), cliente_ejemplo)

def testLoginCliente(setup_client):
    client=setup_client
    response = client.post(
        "/token",
        headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        data={"grant_type": "", "username": cliente_ejemplo["correo"], "password": cliente_ejemplo["password"], "scope": "", 
            "client_id": "", "client_secret": ""}
    )
    print(response.json())
    response_header = {"Authorization": "Bearer "+response.json()["access_token"], "token_type": "bearer"}
    assert response.status_code == 200

def testComprarBillete(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": cliente_ejemplo["correo"], "rol": "cliente"}, expires_delta=access_token_expires)
    
    response = client.post(
        "/billete/nuevo",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json=billete_ejemplo
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), billete_ejemplo, campos=campos_billete)

def testBilletesCliente(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": cliente_ejemplo["correo"], "rol": "cliente"}, expires_delta=access_token_expires)

    response = client.get("/cliente/"+str(billete_ejemplo["cliente"])+"/billetes/",
            headers={"Authorization": "Bearer "+access_token})
    print("Billete cliente ", billete_ejemplo["cliente"])
    print("Billetes cliente ", response.json())
    assert response.status_code == 200
    assert response.json() == [billete_ejemplo]
    

def testSetFechaSalida(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": cliente_ejemplo["correo"], "rol": "cliente"}, expires_delta=access_token_expires)

    response = client.post(
        "/billete/1/cambiar",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        data={"fechaSalida": "2022-05-12T13:00:00.000Z"}
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), billete, campos=campos_billete, exclude=["fechaSalida"])

def testSetFechaLlegada(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": cliente_ejemplo["correo"], "rol": "cliente"}, expires_delta=access_token_expires)

    response = client.post(
        "/billete/1/cambiar",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        data={"fechaLlegada": "2022-05-12T19:00:00.000Z"}
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), billete, campos=campos_billete, exclude=["fechaLlegada"])

def testSetAsiento(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": cliente_ejemplo["correo"], "rol": "cliente"}, expires_delta=access_token_expires)

    response = client.post(
        "/billete/1/cambiar",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        data={"asiento": "65"}
    )
    assert response.status_code == 400
    

def testSetAsiento(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": cliente_ejemplo["correo"], "rol": "cliente"}, expires_delta=access_token_expires)

    response = client.post(
        "/billete/1/cambiar",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        data={"asiento": "30"}
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), billete, campos=campos_billete, exclude=["asiento"])

def testDuracionBillete(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": cliente_ejemplo["correo"], "rol": "cliente"}, expires_delta=access_token_expires)

    response = client.get(
        "/cliente/billete/1/duracion",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200

def testDeleteCliente(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30) #A traves de patch no se cambia la contraseña, se cambia por otro mecanismo
    access_token = crearToken( #Cuando cambia la contraseña se invalida la sesion para iniciar sesion otra vez
    data={"sub": cliente_ejemplo["correo"], "rol": cliente_ejemplo["rol"]}, expires_delta=access_token_expires) #Comprobar el flujo de la sesion

    response = client.delete("/cliente/1/borrar", 
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200

def testDeleteRuta(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30) 
    access_token = crearToken( 
    data={"sub": admin_ejemplo["correo"], "rol": admin_ejemplo["rol"]}, expires_delta=access_token_expires)

    response = client.delete("/ruta/1/borrar", 
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200

def testDeleteAutobus(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30) 
    access_token = crearToken( 
    data={"sub": admin_ejemplo["correo"], "rol": admin_ejemplo["rol"]}, expires_delta=access_token_expires)

    response = client.delete("/autobus/1/borrar", 
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200

def testDeleteViaje(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30) 
    access_token = crearToken( 
    data={"sub": admin_ejemplo["correo"], "rol": admin_ejemplo["rol"]}, expires_delta=access_token_expires)

    response = client.delete("/viaje/1/borrar", 
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200

def testDeleteAdmin(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30) 
    access_token = crearToken( 
    data={"sub": admin_ejemplo["correo"], "rol": admin_ejemplo["rol"]}, expires_delta=access_token_expires)

    response = client.delete("/administrador/1/borrar", 
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200

def testDeleteConductor(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30) 
    access_token = crearToken( 
    data={"sub": conductor_ejemplo["correo"], "rol": conductor_ejemplo["rol"]}, expires_delta=access_token_expires)

    response = client.delete("/conductor/1/borrar", 
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200