#import sys,os
#sys.path.append(os.path.join(os.path.dirname(__file__),os.pardir,"modeloDatos"))
#import myapplib

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import timedelta
import pytest

#from .. import database, main, crud
from ..database import Base
from ..main import app, get_db
from ..crud import getPasswordHash, autenticarUsuario, crearToken
#import database.Base
#import main.app
#import main.get_db
#from .. import database, main

from .aux_func import setup_client

# Datos del usuario sobre el que se realizan las pruebas
datos_ejemplo = {"nombre": "ConductorTest1", "apellidos": "Conductor Test1", "dni": "01234567A", "correo": "conductorTest1@email.com", 
            "password": "test1", "telefono": "6396393639", "rol": "conductor"}

# Campos del usuario que se han de verificar
campos_verificables = ["nombre", "apellidos", "dni", "correo", "telefono", "rol"]

# Función que verifica si dos JSON son iguales en los campos especificados
# saltándose los que se especifiquen en exclude
def verificar_igualdad(user1, user2, campos=campos_verificables, exclude=[]):
    for k in campos:
        if k in exclude:
            continue
        assert user1[k] == user2[k]


# Función que inserta el conductor de datos_ejemplo en la base de datos
def testCrearConductor(setup_client):
    client = setup_client
    response = client.post(
        "/conductor/nuevo",
        #headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        json=datos_ejemplo
    )
    print(response.json())
    assert response.status_code == 200 or response.status_code == 400
    verificar_igualdad(response.json(), datos_ejemplo)

# Función que comprueba el inicio de sesión correcto del conductor creado anteriormente
def testLoginConductor(setup_client):
    client = setup_client
    response = client.post(
        "/token",
        headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        data={"grant_type": "", "username": datos_ejemplo["correo"], "password": datos_ejemplo["password"], "scope": "", 
            "client_id": "", "client_secret": ""}
    )
    print(response.json())
    response_header = {"Authorization": "Bearer "+response.json()["access_token"], "token_type": "bearer"}
    assert response.status_code == 200

# Función que comprueba que los daltos del conductor son correctos
def testVerPerfil(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": datos_ejemplo["correo"], "rol": "conductor"}, expires_delta=access_token_expires)

    response = client.get(
        "/conductores/1/perfil",
        headers={"Authorization": "Bearer "+access_token})
    assert response.status_code == 200
    verificar_igualdad(response.json(), datos_ejemplo)

# Función que comprueba los viajes asignados que tiene el conductor
def testViajesConductor(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": datos_ejemplo["correo"], "rol": "conductor"}, expires_delta=access_token_expires)

    response = client.get("/conductor/1/viajes/",
            headers={"Authorization": "Bearer "+access_token})
    assert response.status_code == 200
    assert response.json()==[]

# Función que comprueba que el nombre del conductor se ha modificado correctamente
def testSetNombre(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": datos_ejemplo["correo"], "rol": "conductor"}, expires_delta=access_token_expires)

    response = client.patch(
        "/conductor/1/editar",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json={"nombre": "ConductorCambio"}
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), datos_ejemplo, exclude=["nombre"])

# Función que comprueba que los apellidos del conductor se han modificado correctamente
def testSetApellidos(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": datos_ejemplo["correo"], "rol": "conductor"}, expires_delta=access_token_expires)

    response = client.patch(
        "/conductor/1/editar",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json={"apellidos": "Conductor Cambio"}
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), datos_ejemplo, exclude=["apellidos"])

# Función que comprueba que el dni del conductor se ha modificado correctamente
def testSetDNI(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": datos_ejemplo["correo"], "rol": "conductor"}, expires_delta=access_token_expires)

    response = client.patch(
        "/conductor/1/editar",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json={"dni": "44332211C"}
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), datos_ejemplo, exclude=["dni"])

# Función que comprueba que el dni del conductor no se modifica por un dni erróneo
def testSetDNIErroneo(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": "conductorViajes@email.com", "rol": "conductor"}, expires_delta=access_token_expires)

    response = client.patch(
        "/conductor/1/editar",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json={"dni": "112233449"}
    )
    assert response.status_code == 422

# Función que compriueba que el dni del conductor no se modifica por uno que no tiene letra
def testSetDNISinLetra(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": datos_ejemplo["correo"], "rol": "conductor"}, expires_delta=access_token_expires)

    response = client.patch(
        "/conductor/1/editar",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json={"dni": "55332211"}
    )
    assert response.status_code == 422

# Función que comprueba que el conductor se elimina de la base de datos correctamente
def testDeleteConductor(setup_client):
    client = setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": datos_ejemplo["correo"], "rol": "conductor"}, expires_delta=access_token_expires)

    response = client.delete("/conductor/1/borrar", 
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200