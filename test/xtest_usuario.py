from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ..database import Base
from ..main import app, get_db
#import database.Base
#import main.app
#import main.get_db
#from .. import database, main

#import unittest

# Crear database
SQLALCHEMY_DATABASE_URL = "mysql+pymysql://uo258740:rom31abi@localhost:3306/db"

engine = create_engine(SQLALCHEMY_DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)

def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()

app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def testCrearUsuario():
    response = client.post(
        "/usuario/",
        #headers={"X-Token": "coneofsilence"},
        json={"nombre": "UsuarioTest1", "apellidos": "Usuario Test1", "dni": "12345678A", "correo": "usuarioTest1@email.com", 
            "password": "test1", "telefono": "654987321"}
    )
    assert response.status_code == 200
    assert response.json() == {
        "nombre": "UsuarioTest1", 
        "apellidos": "Usuario Test1", 
        "dni": "12345678A", 
        "correo": "usuarioTest1@email.com", 
        "password": "test1", 
        "telefono": "654987321"
    }

def testCrearUsuarioMismoDNI():
    response = client.post(
        "/usuario/",
        #headers={"X-Token": "coneofsilence"},
        json={"nombre": "UsuarioTest2", "apellidos": "Usuario Test2", "dni": "12345678A", "correo": "usuarioTest2@email.com", 
            "password": "test2", "telefono": "697410253"}
    )
    assert response.status_code == 400

def testGetUsuario():
    response = client.get("/usuarios/12345678A")
    assert response.status_code == 200
    assert response.json() == {
        "nombre": "UsuarioTest1", 
        "apellidos": "Usuario Test1", 
        "dni": "12345678A", 
        "correo": "usuarioTest1@email.com", 
        "password": "test1", 
        "telefono": "654987321"
    }

def testGetUsuarioDNIErroneo():
    response = client.get("/usuarios/1")
    assert response.status_code == 400
    
def testBilletesUsuarioCreado():
    response = client.get("/usuario/7/billetes/")
    assert response.status_code == 200
    assert response.json()=={}

def testBilletesUsuario():
    response = client.get("/usuario/3/billetes/")
    assert response.status_code == 200
    assert response.json()==[
        {"usuario": "3", "viaje": "5", "fechaSalida": "2022-04-06T10:30:00.000Z", "fechaLlegada": "2022-04-06T14:30:00.000Z", "asiento": "40"},
        {"usuario": "3", "viaje": "6", "fechaSalida": "2022-05-15T19:00:00.000Z", "fechaLlegada": "2022-05-15T23:00:00.000Z", "asiento": "28"}
    ]

def TestSetNombreUsuario():
    client.patch(
        "/usuario/7/editar",
        json={"nombre": "UsuarioTest1Cambio"}
    )
    assert response.status_code == 200
    assert response.json()=={"nombre": "UsuarioTest1Cambio"}

def TestSetApellidosUsuario():
    client.patch(
        "/usuario/7/editar",
        json={"apellidos": "Usuario Cambio"}
    )
    assert response.status_code == 200
    assert response.json()=={"apellidos": "Usuario Cambio"}

def TestSetDNIUsuario():
    client.patch(
        "/usuario/7/editar",
        json={"dni": "11111111B"}
    )
    assert response.status_code == 200
    assert response.json()=={"dni": "11111111B"}

def TestSetDNIErroneoUsuario():
    client.patch(
        "/usuario/7/editar",
        json=={"dni": "5"}
    )
    assert response.status_code == 400

def TestSetCorreoUsuario():
    client.patch(
        "/usuario/7/editar",
        json=={"correo": "usuariocambio@email.com"}
    )
    assert response.status_code == 200
    assert response.json()=={"correo": "usuariocambio@email.com"}

def TestSetPasswordUsuario():
    client.patch(
        "/usuario/7/editar",
        json=={"password": "cambio"}
    )
    assert response.status_code == 200