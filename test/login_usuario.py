import pytest
from fastapi.testclient import TestClient
from ..crud import crearToken, decodeToken, getClienteLogin

@pytest.fixture(scope='session')
def login_usuario():
    """ Esta función crea una sesión para el usuario que inicia sesión y 
        mantiene esa sesión hasta que el usuario la cierra """
