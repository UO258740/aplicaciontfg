#import sys,os
#sys.path.append(os.path.join(os.path.dirname(__file__),os.pardir,"modeloDatos"))
#import myapplib

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import timedelta
import pytest

#from .. import database, main, crud
from ..database import Base
from ..main import app, get_db
from ..crud import getPasswordHash, autenticarUsuario, crearToken
#import database.Base
#import main.app
#import main.get_db
#from .. import database, main

#import unittest

from .aux_func import setup_client

# Datos de los usuarios sobre los que se realizan las pruebas
# Conductores
conductor_ejemplo1 = {"nombre": "ConductorLista1", "apellidos": "Conductor Lista1", "dni": "01234567A", "correo": "conductorLista1@email.com", 
            "password": "lista1", "telefono": '666333999', "rol": "conductor"}
conductor_ejemplo2 = {"nombre": "ConductorLista2", "apellidos": "Conductor Lista2", "dni": "01234567B", "correo": "conductorLista2@email.com", 
            "password": "lista2", "telefono": "666222888", "rol": "conductor"}

# Administradora
admin_ejemplo = {"nombre": "AdminTest", "apellidos": "Admin Test", "dni": "00123456A", "correo": "administradorTest@email.com", 
            "password": "test", "telefono": "663399852", "rol": "administrador"}

# Rutas
ruta_ejemplo1 = {"ciudades": "Gijon Oviedo Leon Madrid"}
ruta_ejemplo2 = {"ciudades": "Valencia Alicante Murcia"}

# Autobuses
autobus_ejemplo1 = {"modelo": "AutobusTest1", "asientos": "45"}
autobus_ejemplo2 = {"modelo": "AutobusTest2", "asientos": "35"}

# Campos del usuario que se han de verificar
campos_verificables = ["nombre", "apellidos", "dni", "correo", "telefono", "rol"]

# Función que verifica si dos JSON son iguales en los campos especificados
# saltándose los que se especifiquen en exclude
def verificar_igualdad(user1, user2, campos=campos_verificables, exclude=[]):
    for k in campos:
        if k in exclude:
            continue
        assert user1[k] == user2[k]


def testCrearConductor1(setup_client):
    client=setup_client
    print("Telefono Conductor 1 ", conductor_ejemplo1["telefono"])
    print("Conductor test ", conductor_ejemplo1)
    response = client.post(
        "/conductor/nuevo",
        #headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        json=conductor_ejemplo1
    )
    print(response.json())
    assert response.status_code == 200 or response.status_code == 400
    verificar_igualdad(response.json(), conductor_ejemplo1)

def testCrearConductor2(setup_client):
    client=setup_client
    response = client.post(
        "/conductor/nuevo",
        #headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        json=conductor_ejemplo2
    )
    print(response.json())
    assert response.status_code == 200 or response.status_code == 400
    verificar_igualdad(response.json(), conductor_ejemplo2)

def testCrearAdministrador(setup_client):
    client=setup_client
    response = client.post(
        "/administrador/nuevo",
        #headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        json=admin_ejemplo
    )
    print(response.json())
    assert response.status_code == 200 or response.status_code == 400
    verificar_igualdad(response.json(), admin_ejemplo)

def testLoginAdministrador(setup_client):
    client=setup_client
    response = client.post(
        "/token",
        headers={"accept": "application/json", "Content-type": "application/x-www-form-urlencoded"},
        data={"grant_type": "", "username": admin_ejemplo["correo"], "password": admin_ejemplo["password"], "scope": "", 
            "client_id": "", "client_secret": ""}
    )
    print(response.json())
    response_header = {"Authorization": "Bearer "+response.json()["access_token"], "token_type": "bearer"}
    assert response.status_code == 200

def testCrearRuta1(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": "administrador"}, expires_delta=access_token_expires)

    response = client.post(
        "/ruta/nueva",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json=ruta_ejemplo1
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), ruta_ejemplo1, campos=["ciudades"])

def testCreaRuta2(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": "administrador"}, expires_delta=access_token_expires)

    response = client.post(
        "/ruta/nueva",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json=ruta_ejemplo2
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), ruta_ejemplo2, campos=["ciudades"])

def testCrearAutobus1(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": "administrador"}, expires_delta=access_token_expires)

    response = client.post(
        "/autobus/nuevo",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json=autobus_ejemplo1
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), autobus_ejemplo1, campos=["modelo", "asientos", "asientosLibres"])

def testCrearAutobus2(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": "administrador"}, expires_delta=access_token_expires)

    response = client.post(
        "/autobus/nuevo",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token},
        json=autobus_ejemplo2
    )
    assert response.status_code == 200
    verificar_igualdad(response.json(), autobus_ejemplo2, campos=["modelo", "asientos", "asientosLibres"])

def testGetConductores(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": "administrador"}, expires_delta=access_token_expires)

    response = client.get(
        "/conductores/",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200
    assert response.json() == [conductor_ejemplo1, conductor_ejemplo2]

def testGetRutas(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": "administrador"}, expires_delta=access_token_expires)

    response = client.get(
        "/rutas/",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200
    assert response.json() == [ruta_ejemplo1, ruta_ejemplo2]

def testDeleteRuta1(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": "administrador"}, expires_delta=access_token_expires)

    response = client.delete(
        "/ruta/1/borrar/",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200

def testDeleteRuta2(setup_client):
    client=setup_client
    access_token_expires = timedelta(minutes=30)
    access_token = crearToken(
    data={"sub": admin_ejemplo["correo"], "rol": "administrador"}, expires_delta=access_token_expires)

    response = client.delete(
        "/ruta/2/borrar/",
        headers={"accept": "application/json", "Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200