from typing import List, Optional
from pydantic import BaseModel
from fastapi import FastAPI, Header
from pydantic import validator
import datetime #from datetime 

class User(BaseModel):
    nombre: str
    apellidos: str
    dni: str
    correo: str
    password: str
    telefono: str
    rol: Optional[str] = Header(None, include_in_schema=False)
    class Config:
        orm_mode = True

    @validator('dni')
    def dni_must_be_valid(cls, v):
        letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        numeros = "1234567890"
        if len(v)!=9:
            raise ValueError("El DNI debe tener 9 caracteres")
        if v[8] not in letras:
            raise ValueError("DNI sin letra")
        for n in v[:8]:
            if n not in numeros:
                raise ValueError("DNI erróneo: {n} no es un número")
        return v

    @validator('telefono')
    def telefono_must_be_valid(cls, v):
        numeros = "6789"
        if len(v)!=9:
            raise ValueError("El teléfono debe tener 9 caracteres")
        if v[0] not in numeros:
            raise ValueError("Número de teléfono erróneo")

class AdministradorBase(User):
    nombre: str
    correo: str
    password: str


class ClienteBase(User):
    nombre: str
    apellidos: str
    dni: str
    correo: str
    password: str
    telefono: str
    #rol: Optional[str] = Header(None, include_in_schema=False)
    #puntos: int
    

class ConductorBase(User):
    nombre: str
    apellidos: str
    dni: str
    correo: str
    password: str
    telefono: str
    #rol: str
    

class AutobusBase(BaseModel):
    modelo: str
    asientos: int
    #asientosLibres: int
    

class RutaBase(BaseModel):
    #numeroCiudades: int
    ciudades: str


class ViajeBase(BaseModel):
    conductor: int
    autobus: int
    #duracion: int
    ruta: int
    precio: float


class BilleteBase(BaseModel):
    cliente: int
    viaje: int
    fechaSalida: datetime.datetime
    fechaLlegada: datetime.datetime
    asiento: int




class AdministradorFull(AdministradorBase):
    id: int
    nombre: Optional[str] = None
    correo: Optional[str] = None
    #password: Optional[str] = None
    rol: Optional[str] = Header(None, include_in_schema=False)
    class Config:
        orm_mode = True


class ClienteFull(ClienteBase):
    id: int
    nombre: Optional[str] = None
    apellidos: Optional[str] = None
    dni: Optional[str] = None
    correo: Optional[str] = None
    #password: Optional[str] = None
    telefono: Optional[str] = None
    puntos: Optional[int] = None # long?
    rol: Optional[str] = Header(None, include_in_schema=False)
    class Config:
        orm_mode = True

    @validator('dni')
    def dni_must_be_valid(cls, v):
        letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        numeros = "1234567890"
        if len(v)!=9:
            raise ValueError("El DNI debe tener 9 caracteres")
        if v[8] not in letras:
            raise ValueError("DNI sin letra")
        for n in v[:8]:
            if n not in numeros:
                raise ValueError("DNI erróneo: {n} no es un número")
        return v

    @validator('telefono')
    def telefono_must_be_valid(cls, v):
        numeros = "6789"
        if len(v)!=9:
            raise ValueError("El teléfono debe tener 9 caracteres")
        if v[0] not in numeros:
            raise ValueError("Número de teléfono erróneo")
        return v



class ConductorFull(ConductorBase):
    id: int
    nombre: Optional[str] = None
    apellidos: Optional[str] = None
    dni: Optional[str] = None
    correo: Optional[str] = None
    #password: Optional[str] = None
    telefono: Optional[str] = None
    rol: Optional[str] = Header(None, include_in_schema=False)
    class Config:
        orm_mode = True

    @validator('dni')
    def dni_must_be_valid(cls, v):
        print("dni Schema ", v)
        letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        numeros = "1234567890"
        if len(v)!=9:
            raise ValueError("El DNI debe tener 9 caracteres")
        if v[8] not in letras:
            raise ValueError("DNI sin letra")
        for n in v[:8]:
            if n not in numeros:
                raise ValueError("DNI erróneo: {n} no es un número")
        return v

    @validator('telefono')
    def telefono_must_be_valid(cls, v):
        print("Telefono Schema ", v)
        numeros = "6789"
        if len(v)!=9:
            raise ValueError("El teléfono debe tener 9 caracteres")
        if v[0] not in numeros:
            raise ValueError("Número de teléfono erróneo")
        return v


class AutobusFull(AutobusBase):
    id: int
    modelo: Optional[str] = None
    asientos: Optional[int] = 1
    asientosLibres: Optional[int] = 1
    class Config:
        orm_mode = True


class RutaFull(RutaBase):
    id: int
    numeroCiudades: Optional[int] = 0
    ciudades: Optional[str] = None
    class Config:
        orm_mode = True


class ViajeFull(ViajeBase):
    id: int
    conductor: Optional[int] = None
    autobus: Optional[int] = None
    #duracion: Optional[int] = None
    ruta: Optional[int] = None
    precio: Optional[float] = None
    class Config:
        orm_mode = True


class BilleteFull(BilleteBase):
    id: Optional[int] = None
    cliente: Optional[int] = None
    viaje: Optional[int] = None
    fechaSalida: Optional[datetime.datetime] = None
    fechaLlegada: Optional[datetime.datetime] = None
    asiento: Optional[int] = None
    class Config:
        orm_mode = True



class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    usuario: Optional[str] = None
    rol: Optional[str] = None


#class ClienteCreate(ClienteBase):
#    password: str
#    telefono: str
#    pass


#class ConductorCreate(ConductorBase):
#    password: str
#    telefono: str
#    pass


#class AutobusCreate(AutobusBase):
#    pass


#class ItinerarioCreate(ItinerarioBase):
#    numeroCiudades: int
#    ciudades: str
#    pass


#class RutaCreate(RutaBase):
#    conductor: int
#    autobus: int
#    pass


#class ViajeCreate(ViajeBase):
#    pass

#class ClienteUpdate(ClienteBase):
#    password: Optional[str] = None
#    puntos: int # long?
#    pass

#class RutaUpdate(RutaBase):
#    fechaSalida: datetime
#    fechaLlegada: datetime
#    precio: float
#    conductor: int
#    autobus: int
#    itinerario: int
#    pass

#class ItinerarioUpdate(ItinerarioBase):
#    numeroCiudades: int
#    ciudades: str
#    pass

#class ViajeUpdate(ViajeBase):
#    asiento: int
#    pass

